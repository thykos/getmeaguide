import gulp from 'gulp';
import {BowerDir} from './dist';
import concat from 'gulp-concat';
import {PublicScripts, PublicStyles} from './dist';
import plumber from 'gulp-plumber';
import errorsHandler from './error';

export const depScripts = [
  `${BowerDir}/jquery/dist/jquery.js`,
  `${BowerDir}/bootstrap/dist/js/bootstrap.js`
];

export const depStyles = [
  `${BowerDir}/bootstrap/dist/css/bootstrap.min.css`,
  `${BowerDir}/font-awesome/css/font-awesome.min.css`
];

gulp.task('concatDepScripts', () =>
  gulp.src(depScripts)
    .pipe(plumber({errorHandler: errorsHandler('Dep Scripts')}))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest(PublicScripts))
);

export const addDepScripts = gulp.task('addDepScripts', ['concatDepScripts'], () =>
  gulp.watch(depScripts, ['concatDepScripts'])
);

gulp.task('concatDepStyles', () =>
  gulp.src(depStyles)
    .pipe(plumber({errorHandler: errorsHandler('Dep Styles')}))
    .pipe(concat('depStyles.css'))
    .pipe(gulp.dest(PublicStyles))
);

export const addDepStyles = gulp.task('addDepStyles', ['concatDepStyles'], () =>
  gulp.watch(depStyles, ['concatDepStyles'])
);
