import gulp from 'gulp';
import slim from 'gulp-slim';
import { Slims, Public, SlimsComponents } from './dist';
import { reload } from './server';
import plumber from 'gulp-plumber';
import {stream as wiredep} from 'wiredep';
import errorsHandler from './error';
import bowerConfigs from './bowerConfigs.json';

gulp.task('compileSlim', () =>
  gulp.src(Slims)
    .pipe(plumber({errorHandler: errorsHandler('Slim')}))
    .pipe(slim({
      require: 'slim/include',
      options: `include_dirs=["${SlimsComponents}"]`,
      pretty: true
    }))
    .pipe(wiredep(bowerConfigs))
    .pipe(gulp.dest(Public))
    .pipe(reload())
);

export default gulp.task('slim', ['compileSlim'], () =>
  gulp.watch(Slims, ['compileSlim'])
);
