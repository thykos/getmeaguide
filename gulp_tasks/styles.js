import gulp from 'gulp';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import concat from 'gulp-concat';
import {Styles, PublicStyles} from './dist';
import {reload} from './server';
import plumber from 'gulp-plumber';
import errorsHandler from './error';

gulp.task('compileStyles', () =>
  gulp.src(Styles)
    .pipe(plumber({errorHandler: errorsHandler('Styles')}))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', function(err) {
      console.log(err.message);
      this.end();
    })
    .pipe(concat('styles.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(PublicStyles))
    .pipe(reload())
);

export default gulp.task('styles', ['compileStyles'], () =>
  gulp.watch(Styles, ['compileStyles'])
);
